package com.glearning.orders.util;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunner implements CommandLineRunner {
	
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void run(String... args) throws Exception {
		/*
		 * System.out.println("Hello world from Spring Boot-2 !!"); String[]
		 * beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
		 * Stream.of(beanDefinitionNames).forEach(bean -> System.out.println(bean));
		 */
		
	}

}

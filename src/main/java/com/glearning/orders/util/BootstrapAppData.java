package com.glearning.orders.util;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.glearning.orders.model.Order;
import com.glearning.orders.service.OrderService;

@Component
public class BootstrapAppData implements CommandLineRunner {

	@Autowired
	private OrderService orderService;

	@Override
	public void run(String... args) throws Exception {

		for (int i = 0; i < 10; i++) {
			// Order order = new Order(12, 22,"Mary","John", true, true, 0 , 34);
			Order order = Order.builder().date(LocalDate.now()).amount(4000).modeOfPayment("UPI").name("Hari").build();
			this.orderService.save(order);
		}
	}

}

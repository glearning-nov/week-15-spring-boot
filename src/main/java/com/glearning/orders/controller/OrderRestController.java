package com.glearning.orders.controller;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.glearning.orders.model.Order;
import com.glearning.orders.service.OrderService;

@RestController
//complete url will be http://<host-name/ip-adddress>:<port-number>/api/orders
@RequestMapping("/api/orders")
public class OrderRestController {
	
	@Autowired
	private OrderService orderService;
	
	//http://<host-name/ip-adddress>:<port-number>/api/orders
	@GetMapping
	public Set<Order> fetchAllOrders(){
		return this.orderService.fetchAllOrders();
	}

	//http://<host-name/ip-adddress>:<port-number>/api/orders
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order createOrder(@RequestBody Order order) {
		System.out.println("for testing");
		return this.orderService.save(order);
	}
	
	//http://<host-name/ip-adddress>:<port-number>/api/orders/id
	//path variable
	@GetMapping("/{id}")
	public Order findOrderById(@PathVariable long id) {
		return this.orderService.findOrderByOrderId(id);
	}
	
	
	@PutMapping("/{id}")
	public Order updateOrder(@PathVariable long id, @RequestBody Order order) {
		return null;
	}

	//http://<host-name/ip-adddress>:<port-number>/api/orders/id
	//path variable
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable("id") long orderId) {
		this.orderService.deleteOrderById(orderId);
	}
}

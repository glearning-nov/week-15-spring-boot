package com.glearning.orders.service;

import java.util.Set;

import com.glearning.orders.model.Order;

public interface OrderService {
	
	Order save(Order order);
	
	Set<Order> fetchAllOrders();
	
	Order findOrderByOrderId(long orderId);
	
	Order updateOrderById(long orderId);

	void deleteOrderById(long orderId);
}

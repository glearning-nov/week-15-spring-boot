package com.glearning.orders.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.orders.dao.OrderJpaRepository;
import com.glearning.orders.model.Order;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderJpaRepository orderRepository;

	@Override
	//perform any business logic here
	public Order save(Order order) {
		Order savedOrder = this.orderRepository.save(order);
		return savedOrder;
	}

	@Override
	public Set<Order> fetchAllOrders() {
		Set<Order> orders = Set.copyOf(this.orderRepository.findAll());
		return orders;
	}

	@Override
	public Order findOrderByOrderId(long orderId) {
		Optional<Order> optionalOrder = this.orderRepository.findById(orderId);
		Order fetchedOrder = optionalOrder.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
		return fetchedOrder;
	}

	@Override
	public Order updateOrderById(long orderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}
}
